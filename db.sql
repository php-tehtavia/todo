create table task(
    id int auto_increment primary key,
    description varchar(255) not null,
    done boolean default false,
    added TIMESTAMP default CURRENT_TIMESTAMP
)